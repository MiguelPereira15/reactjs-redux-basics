import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {connect} from "react-redux";

import {User} from "./components/User";
import {Main} from "./components/Main";
import {setName, setAge} from "./actions/userActions";
import {addNumber, subtractNumber} from "./actions/mathActions";

import { throws } from 'assert';

class App extends Component {
  render() {
    return (
      <div className="container">
        <Main changeUsername={() => this.props.setName("Anna")}/>
        <User username={this.props.user.name}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    math: state.math  
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setName: (name) => {
      dispatch(setName(name));
    },
    setAge: (age) => {
      dispatch(setAge(age));
    },
    add: (number) => {
      dispatch(addNumber(number));
    },
    subtract: (number) => {
      dispatch(subtractNumber(number));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

//export default App;